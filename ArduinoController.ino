int inPins[] = { 8 };
int inPinLength;
bool inPinsStatus[sizeof(inPins)];

void setup() {

  inPinLength = sizeof(inPins)/sizeof(int);
 
  for (int i=0; i <= inPinLength; i++){
     pinMode(inPins[i], INPUT);
  } 

  Serial.begin(9600);
}

void loop() {

  for (int i = 0; i < inPinLength; i++){
    int val = digitalRead(inPins[i]);
     if (val == HIGH){
        if (inPinsStatus[i] == false){        
            inPinsStatus[i] = true;
            Serial.write(inPins[i]);
        }
     } else {
        if (inPinsStatus[i] == true){        
            inPinsStatus[i] = false;
            Serial.write(inPins[i] + 100);
        }
     }
  } 
}
